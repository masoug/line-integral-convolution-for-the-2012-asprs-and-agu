import sys
import time
import bpy
import bmesh
from mathutils import *

print("Importing ASCII DEM file...")

# Open file and read header.
print("Reading file...")
dem = open("C:\\Projects\\2012_Summer_AlaskaClimate\\Animation\\Terrain\\DEM\\srtm_04_02.asc", "r")
ncols = int(dem.readline().split()[1])
print("Number of columns:", ncols)
nrows = int(dem.readline().split()[1])
print("Number of rows:", nrows)
if ncols != nrows:
    print("Rows and columns do not match.")
print(dem.readline())
print(dem.readline())
print(dem.readline())
nodata = int(dem.readline().split()[1])
print("Nodata:", nodata)

# Load into memory.
print("Loading DEM, may take forever (or all your memory)...")
points = []
stride = 1
scale = 0.1
for i in range(int(nrows/stride)):
    row = dem.readline().split()
    for j in range(int(len(row)/stride)):
        if int(row[j*stride]) != -9999:
            #print("Appending:", scale*i, scale*j, scale*int(row[j*stride]))
            points.append((scale*i, scale*j, 0.0001*int(row[j*stride])))
    
    
    # Dumb way to skip...
    if stride > 1:
            for meh in range(stride):
                dem.readline()

#print(points)
print("Appended ", len(points))

# Creating the mesh.
print("Creating mesh...")
mesh = bpy.data.meshes.new("TerrainMesh")
obj = bpy.data.objects.new("Terrain", mesh)
bpy.context.scene.objects.link(obj)
mesh.from_pydata(points,[], [])
mesh.update(calc_edges=True)    # Update mesh with new data
"""
for vert in mesh.vertices:
    print(vert.co)
"""
# Close DEM file descriptor.
dem.close()

print("Done.")