# This is a script that does stuff.

from __future__ import division
import Image
import math
import random
import copy

# Reads datafile and returns array of data.
def ReadData(filename):
    #Read data and stores them in memory.
    data = open(filename, "r")
    print "========== Header Information =========="
    print data.readline()
    print data.readline()
    dim = data.readline()
    print dim
    print data.readline()
    print data.readline()
    print "========================================"
    print "Image dimensions:"
    dim = dim.split()
    samples = int(dim[3])
    lines = int(dim[6])
    size = lines*samples
    print "Samples:", samples, "Lines:", lines, "Total size:", size
    x = []
    for i in range(size):
        line = data.readline().split()
        x.append([float(line[0]), float(line[1]), float(line[2])])
    data.close()
    return [x, samples, lines]

# Returns a rgb tuple to given a magnitude.
def rgbmag(mag):
    r = 0
    g = 0
    b = 0
    if mag >= 0.4:
        g = 0.4
        b = 0
    elif mag <= 0:
        g = 0
        b = 0.4
    else:
        g = mag
        b = 0.4-mag
    return (float(r), float(g), float(b))

# Gets magnitude.
def mag(x, y):
    return math.sqrt(x*x + y*y)

# Main function.
if __name__ == "__main__":
    
    # Configuration.
    print "Velocity Map"
    xfilename = "C:\\Projects\\2012_Summer_AlaskaClimate\\Animation\\Vmap\\Trial\\SW_X_2.txt"
    yfilename = "C:\\Projects\\2012_Summer_AlaskaClimate\\Animation\\Vmap\\Trial\\SW_Y_2.txt"
    #xfilename = "F:\\glacier_asciis\\e_x.txt"
    #yfilename = "F:\\glacier_asciis\\e_y.txt"
    #xfilename = "xvals.txt"
    #yfilename = "yvals.txt"
    #errorfilename = "C:\\Projects\\2012_Summer_AlaskaClimate\\Animation\\Vmap\\Trial\\SW_SNR.txt"
    print "X direction datafile:", xfilename
    print "Y direction datafile:", yfilename
    #print "Error datafile:", errorfilename
    
    # Read data.
    print "Reading datafiles..."
    xdata = ReadData(xfilename)
    ydata = ReadData(yfilename)

    img = Image.new("RGB", (xdata[1], xdata[2]))
    for j in range(xdata[2]):
        for i in range(xdata[1]):
            img.putpixel((i, j), rgbmag(mag(7*xdata[0][j*xdata[1]+i][2], 7*ydata[0][j*ydata[1]+i][2])))
    img.save("Magnitude.png")
    
    print "Done."
