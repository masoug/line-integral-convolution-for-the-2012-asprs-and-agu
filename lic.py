# This is a script that does stuff.

from __future__ import division
import Image
import math
import random
import copy
import velocity

# Reads datafile and returns array of data.
def ReadData(filename):
    #Read data and stores them in memory.
    data = open(filename, "r")
    print "========== Header Information =========="
    print data.readline()
    print data.readline()
    dim = data.readline()
    print dim
    print data.readline()
    print data.readline()
    print "========================================"
    print "Image dimensions:"
    dim = dim.split()
    samples = int(dim[3])
    lines = int(dim[6])
    size = lines*samples
    print "Samples:", samples, "Lines:", lines, "Total size:", size
    x = []
    for i in range(size):
        line = data.readline().split()
        x.append([float(line[0]), float(line[1]), float(line[2])])
    data.close()
    return [x, samples, lines]

# Gets value from vector given PIXEL coordinates.
# "pos" is a tuple for (x-pos, y-pos).
def getval(pos, data):
    samp = data[1]
    lines = data[2]
    # Check signs!
    if pos[0] < 0 or pos[1] < 0:
        return 0
    # Check range!
    elif pos[0] > samp-1 or pos[1] > lines-1:
        return 0
    else:
        return data[0][(samp*pos[1])+pos[0]][2]

# Calculate integral curve (LIFK).
def lifk(pos, xdata, ydata, ds, L):
    C = [] # Pathline
    C.append(tuple(pos))
    x = int(pos[0])
    y = int(pos[1])
    for s in range(L): # Positive direction.
        dx = getval((int(x), int(y)), xdata)
        dy = -getval((int(x), int(y)), ydata) # Y-values are weird for some reason...
        x += ds*dx
        y += ds*dy
        C.append(tuple((int(x), int(y))))
    x = int(pos[0])
    y = int(pos[1])
    for s in range(L): # Negative direction.
        dx = getval((int(x), int(y)), xdata)
        dy = -getval((int(x), int(y)), ydata) # Y-values are weird for some reason...
        x -= ds*dx
        y -= ds*dy
        C.append(tuple((int(x), int(y))))
    return C

# Check image bounds.
def inbounds(pos, img):
    # Check signs!
    if pos[0] < 0 or pos[1] < 0:
        return False
    # Check range!
    if pos[0] >= img.size[0] or pos[1] >= img.size[1]:
        return False
    return True

# Hanning filter function.
def hanning(s, ds, bt):
    a = s
    b = s + ds
    c = 1
    d = 6.5
    beta = bt
    result = float(b-a)
    result += (math.sin(b*c)-math.sin(a*c))/c
    result += (math.sin(b*d+beta)-math.sin(a*d+beta))/d
    result += (math.sin(b*(c-d)-beta)-math.sin(a*(c-d)-beta))/(2*(c-d))
    result += (math.sin(b*(c+d)+beta)-math.sin(a*(c+d)+beta))/(2*(c+d))
    return 0.25*result

# Convolution.
# Convolve it like its 1999!
def conv(noise, C, ds, pha):
    acc = 0
    han = 0
    for i, p in enumerate(C):
        if inbounds(p, noise):
            acc += hanning(i*ds, ds, pha)*noise.getpixel((p[0], p[1]))
            han += hanning(i*ds, ds, pha)
        else:
            acc += 0
    acc = acc/han
    #print acc
    return acc

# LIC.
def lic(outname, noise, xdata, ydata, ds, L, pha):
    img = Image.new("RGB", (xdata[1], xdata[2]))
    for j in range(img.size[1]):
        #print "Processing line", j
        for i in range(img.size[0]):
            mag = velocity.rgbmag(velocity.mag(0.01*xdata[0][j*xdata[1]+i][2], 0.01*ydata[0][j*ydata[1]+i][2]))
            con = conv(noise, lifk((i, j), xdata, ydata, ds, L), ds, pha)
            img.putpixel((i, j), (int((1+mag[0])*con), int((1+mag[1])*con), int((1+mag[2])*con*2)))
    img.save(outname)

# Main function.
if __name__ == "__main__":
    
    # Configuration.
    print "Line Integral Convolution"
    #xfilename = "C:\\Projects\\2012_Summer_AlaskaClimate\\Animation\\Vmap\\Trial\\SW_X_2.txt"
    #yfilename = "C:\\Projects\\2012_Summer_AlaskaClimate\\Animation\\Vmap\\Trial\\SW_Y_2.txt"
    xfilename = "H:\\e_smoothed_x.txt"
    yfilename = "H:\\e_smoothed_y.txt"
    #xfilename = "xvals.txt"
    #yfilename = "yvals.txt"
    #errorfilename = "C:\\Projects\\2012_Summer_AlaskaClimate\\Animation\\Vmap\\Trial\\SW_SNR.txt"
    print "X direction datafile:", xfilename
    print "Y direction datafile:", yfilename
    #print "Error datafile:", errorfilename
    
    # Read data.
    print "Reading datafiles..."
    xdata = ReadData(xfilename)
    ydata = ReadData(yfilename)

    # Create random noise!
    print "Generating random noise..."
    noise = Image.new("L", (xdata[1], xdata[2]))
    for j in range(noise.size[1]):
        for i in range(noise.size[0]):
            noise.putpixel((i, j), random.randrange(0, 255))
    noise.save("Noise.png")

    # Perform LIC.
    print "Applying LIC..."
    for t in range(10):
        print "Rendering frame", t
        lic("elic"+str(t)+".png", noise, xdata, ydata, 0.1, 15, (6.28/10)*t)
    
    print "Done."
